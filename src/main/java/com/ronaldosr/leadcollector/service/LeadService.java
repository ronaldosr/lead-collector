package com.ronaldosr.leadcollector.service;

import com.ronaldosr.leadcollector.DTO.RequestIdProdutoDTO;
import com.ronaldosr.leadcollector.DTO.RequestLeadDTO;
import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.model.Produto;
import com.ronaldosr.leadcollector.repository.LeadRepository;
import com.ronaldosr.leadcollector.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.misc.Request;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead salvarLead(RequestLeadDTO requestLeadDTO) {
        return leadRepository.save(converterDtoParaLead(requestLeadDTO));
    }

    public List<Lead> lerTodosLeads() {
        return leadRepository.findAll();
    }

    public Lead lerLeadPorId(int id) {
        Optional<Lead> leadOptional = leadRepository.findById(id);
        if (leadOptional.isPresent()) {
            return leadOptional.get();
        } else {
            throw new RuntimeException("Registro não encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead) {
        Lead leadCadastrado = lerLeadPorId(id);
        lead.setId(leadCadastrado.getId());
        return leadRepository.save(lead);
    }

    public void excluirLead(int id) {
        if (leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Lead converterDtoParaLead(RequestLeadDTO requestLeadDTO) {
        Lead lead = new Lead();
        lead.setCpf(requestLeadDTO.getCpf());
        lead.setNome(requestLeadDTO.getNome());
        lead.setEmail(requestLeadDTO.getEmail());
        lead.setTelefone(requestLeadDTO.getTelefone());
        lead.setProdutos(converterIdParaProduto(requestLeadDTO.getIdProdutos()));
        return lead;
    }

    private List<Produto> converterIdParaProduto(List<RequestIdProdutoDTO> idProdutos) {
        List<Produto> produtos = new ArrayList<>();
        for (RequestIdProdutoDTO idProdutoDTO : idProdutos) {
            Optional<Produto> produto = produtoRepository.findById(idProdutoDTO.getId());
            if (produto.isPresent()) {
                produtos.add(produto.get());
            }
        }
        return produtos;
    }

    public List<Lead> pesquisarPorCPF(String cpf) {
        List<Lead> leads = leadRepository.findByCpf(cpf);

        if (!leads.isEmpty()) {
            return leads;
        } else {
            throw  new RuntimeException("Cpf não encontrado");
        }
    }

    public List<Lead> pesquisarPorEmail(String email) {
        List<Lead> leads = leadRepository.findByEmail(email);

        if (!leads.isEmpty()) {
            return leads;
        } else {
            throw  new RuntimeException("E-mail não encontrado");
        }
    }

}
