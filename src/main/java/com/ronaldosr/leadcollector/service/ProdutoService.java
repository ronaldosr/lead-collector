package com.ronaldosr.leadcollector.service;

import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.model.Produto;
import com.ronaldosr.leadcollector.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public List<Produto> lerTodosProdutos() {
        return produtoRepository.findAll();
    }

    public Produto lerProdutoPorId(int id) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if (produtoOptional.isPresent()) {
            return produtoOptional.get();
        } else {
            throw new RuntimeException("Registro não encontrado");
        }
    }

    public Produto atualizarProduto(int id, Produto produto) {
        Produto produtoCadastrado = lerProdutoPorId(id);
        produto.setId(produtoCadastrado.getId());
        return produtoRepository.save(produto);
    }

    public void excluirLead(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }

    }
}
