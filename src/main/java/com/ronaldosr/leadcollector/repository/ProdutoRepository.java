package com.ronaldosr.leadcollector.repository;

import com.ronaldosr.leadcollector.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository <Produto, Integer> {

}
