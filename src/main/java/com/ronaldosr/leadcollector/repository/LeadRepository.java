package com.ronaldosr.leadcollector.repository;

import com.ronaldosr.leadcollector.model.Lead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface LeadRepository extends JpaRepository<Lead, Integer> {

    // Habilita a escrita na query
    //@Modifying
    //@Query(value = "INSERT INTO lead (nome, cpf, blablabla ) values :lead.get...")
    //Lead inserirLead(Lead lead);

    List<Lead> findByCpf(String cpf);

    //List<Lead> findByCpfAndNome(String cpf, String nome);

    // O "L" de lead deve ser maísculo, pois representa o nome da CLASSE!!!
    @Query(value = "SELECT l FROM Lead l WHERE l.email = :email")
    List<Lead> findByEmail(String email);
}
