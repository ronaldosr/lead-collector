package com.ronaldosr.leadcollector.DTO;

import com.ronaldosr.leadcollector.model.Lead;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ResponseLeadDTO {

    private int id;
    private String nome;
    private String descricao;
    private BigDecimal preco;

    public ResponseLeadDTO() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public List<ResponseLeadDTO> listaResponseLeadDTO (List<Lead> leads) {
        List<ResponseLeadDTO> listaResponseLeadDTO = new ArrayList<>();
        for (Lead lead : leads) {
            ResponseLeadDTO responseLeadDTO = new ResponseLeadDTO();
            responseLeadDTO.setId(lead.getId());

        }
        return listaResponseLeadDTO;
    }
}
