package com.ronaldosr.leadcollector.DTO;

import com.ronaldosr.leadcollector.model.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class RequestLeadDTO {

    @NotEmpty(message = "Nome inválido")
    @Size(min = 3, max = 50, message = "O nome deve ter entre 3 e 50 caracteres")
    private String nome;

    @CPF(message = "CPF inválido")
    @NotNull
    private String cpf;

    @Email(message = "E-mail inválido")
    @NotNull
    private String email;

    @NotNull
    @Size(min = 10, max = 10, message = "O telefone deve estar no fomato 99999-9999")
    private String telefone;

    @NotNull
    List<RequestIdProdutoDTO> idProdutos;

    public RequestLeadDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<RequestIdProdutoDTO> getIdProdutos() {
        return idProdutos;
    }

    public void setIdProdutos(List<RequestIdProdutoDTO> idProdutos) {
        this.idProdutos = idProdutos;
    }
}
