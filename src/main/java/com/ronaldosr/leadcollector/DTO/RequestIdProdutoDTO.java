package com.ronaldosr.leadcollector.DTO;

public class RequestIdProdutoDTO {

    private int id;

    public RequestIdProdutoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
