package com.ronaldosr.leadcollector.controller;

import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.model.Produto;
import com.ronaldosr.leadcollector.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto salvar(@RequestBody @Valid Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public List<Produto> lerTodosProdutos(){
        return produtoService.lerTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto lerProdutoPorId(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.lerProdutoPorId(id);
            return produto;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") int id, @Valid @RequestBody Produto produto) {
        try {
            Produto produtoCadastrado = produtoService.atualizarProduto(id, produto);
            return produtoCadastrado;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirLead(@PathVariable(name = "id") int id) {
        try {
            produtoService.excluirLead(id);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
