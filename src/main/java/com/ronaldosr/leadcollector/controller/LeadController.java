package com.ronaldosr.leadcollector.controller;

import com.ronaldosr.leadcollector.DTO.RequestLeadDTO;
import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead salvar(@RequestBody @Valid RequestLeadDTO requestLeadDTO) {
        return leadService.salvarLead(requestLeadDTO);
    }

    @GetMapping
    public List<Lead> lerTodosLeads(){
        return leadService.lerTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead lerLeadPorId(@PathVariable(name = "id") int id) {
        try {
            Lead lead = leadService.lerLeadPorId(id);
            return lead;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable(name = "id") int id, @Valid @RequestBody Lead lead) {
        try {
            Lead leadCadastrado = leadService.atualizarLead(id, lead);
            return leadCadastrado;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirLead(@PathVariable(name = "id") int id) {
        try {
            leadService.excluirLead(id);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/detalhes")
    public List<Lead> pesquisarPorCpf (@RequestParam(name = "cpf") String cpf) {
        try {
            return leadService.pesquisarPorCPF(cpf);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/detalhes2")
    public List<Lead> pesquisarPorEmail (@RequestParam(name = "email") String email) {
        try {
            return leadService.pesquisarPorEmail(email);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
