package com.ronaldosr.leadcollector.model;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "lead")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome", length = 50)
    @NotEmpty (message = "Nome inválido")
    private String nome;

    @Column(name = "cpf", length = 14)
    @CPF(message = "CPF inválido")
    @NotNull
    private String cpf;

    @Email(message = "E-mail inválido")
    @Column(name = "email", length = 50)
    @NotNull
    private String email;

    @Column(name = "telefone", length = 10)
    @NotNull
    private String telefone;

    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name="lead_produtos",
//            joinColumns = {@JoinColumn(name = "produtos_id")},
//            inverseJoinColumns = {@JoinColumn(name = "lead_id")})
    private List<Produto> produtos;

    public Lead() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Lead{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                ", produtos=" + produtos +
                '}';
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

}
