package com.ronaldosr.leadcollector.model;

import sun.nio.fs.GnomeFileTypeDetector;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.concurrent.LinkedTransferQueue;

@Entity
@Table(name = "produto")
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome", length = 40)
    @Size(min = 3, max = 40, message = "O nome deve ter entre 3 e 40 caracteres")
    @NotEmpty(message = "Nome inválido")
    private String nome;

    @Column(name = "descricao", length = 50)
    @Size(min = 3, max = 50, message = "A descrição deve ter entre 3 e 50 caracteres")
    @NotEmpty(message = "Descrição inválida")
    private String descricao;

    @Column(name = "preco")
    @NotNull
    @Digits(integer = 8, fraction = 2, message = "Preço inválido")
    @DecimalMin(value = "0.01", message = "O preço mínimo é de R$ 0,01")
    private BigDecimal preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Produto{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", preco=" + preco +
                '}';
    }
}
