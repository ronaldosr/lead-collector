package com.ronaldosr.leadcollector.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ronaldosr.leadcollector.DTO.RequestIdProdutoDTO;
import com.ronaldosr.leadcollector.DTO.RequestLeadDTO;
import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.model.Produto;
import com.ronaldosr.leadcollector.service.LeadService;
import org.apache.catalina.webresources.JarResource;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    List<Lead> leads;
    List<RequestIdProdutoDTO> requestIdProdutoDTOs;
    RequestLeadDTO requestLeadDTO;

    @BeforeEach
    private void setUp() {
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Lead Teste");
        lead.setTelefone("99999-9999");
        lead.setCpf("572.850.230-00");
        lead.setEmail("teste@gmail.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto Teste");
        produto.setDescricao("Descrição do produto teste");
        produto.setPreco(new BigDecimal(100.00));

        List<Produto> produtos = Arrays.asList(produto);
        lead.setProdutos(produtos);

        this.leads = Arrays.asList(lead);

        this.requestLeadDTO = new RequestLeadDTO();
        requestLeadDTO.setCpf("572.850.230-00");
        requestLeadDTO.setTelefone("999999");
        requestLeadDTO.setEmail("joa@email.com");
        requestLeadDTO.setNome("Teste");

        RequestIdProdutoDTO requestIdProdutoDTOs = new RequestIdProdutoDTO();
        requestIdProdutoDTOs.setId(1);

        List<RequestIdProdutoDTO> idProdutoDTOS = Arrays.asList(requestIdProdutoDTOs);
        requestLeadDTO.setIdProdutos(idProdutoDTOS);


    }

    @Test
    public void testarBuscaPorId() throws Exception {
        Mockito.when(leadService.lerLeadPorId(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscaPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.lerLeadPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/ " + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarTodosOsLeads() throws Exception {
        List<Lead> leads = new ArrayList<>();
        Mockito.when(leadService.lerTodosLeads()).thenReturn(leads);

       mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(RequestLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(requestLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$", CoreMatchers.equalTo("Teste")));
    }

    @Test
    public void  testarValidacaoDeCadastroDeLead() {
        requestLeadDTO.setCpf("888888888888");
        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper
    }


}
