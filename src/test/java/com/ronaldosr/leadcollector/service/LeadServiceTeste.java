package com.ronaldosr.leadcollector.service;

import com.ronaldosr.leadcollector.DTO.RequestIdProdutoDTO;
import com.ronaldosr.leadcollector.DTO.RequestLeadDTO;
import com.ronaldosr.leadcollector.model.Lead;
import com.ronaldosr.leadcollector.model.Produto;
import com.ronaldosr.leadcollector.repository.LeadRepository;
import com.ronaldosr.leadcollector.repository.ProdutoRepository;
import com.sun.xml.internal.ws.policy.AssertionSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import sun.misc.Request;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    @Autowired
    private ProdutoService produtoService;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void setUp() {
       this.lead = new Lead();
       lead.setId(1);
       lead.setNome("Lead Teste");
       lead.setTelefone("99999-9999");
       lead.setCpf("572.850.230-00");
       lead.setEmail("teste@gmail.com");

       this.produto = new Produto();
       produto.setId(1);
       produto.setNome("Produto Teste");
       produto.setDescricao("Descrição do produto teste");
       produto.setPreco(new BigDecimal(100.00));

       List<Produto> produtos = Arrays.asList(produto);
       lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscaDeLeadPeloId() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        //Lead leadObjeto = leadService.lerLeadPorId(Mockito.anyInt());
        Lead leadObjeto = leadService.lerLeadPorId(1000);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getNome(), leadObjeto.getNome());
    }

    @Test
    public void testarBuscaDeLeadPeloIdQueNaoExiste() {
        Optional<Lead> leadOptional = Optional.empty();

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.lerLeadPorId(Mockito.anyInt());
        });
    }

    @Test
    public void testarSalvaLead() {
        RequestLeadDTO requestLeadDTO = new RequestLeadDTO();
        requestLeadDTO.setCpf("572.850.230-00");
        requestLeadDTO.setNome("Lead de Teste");
        requestLeadDTO.setEmail("teste@teste.com.br");
        requestLeadDTO.setTelefone("99999-9999");

        RequestIdProdutoDTO requestIdProdutoDTO = new RequestIdProdutoDTO();
        requestIdProdutoDTO.setId(1);

        List<RequestIdProdutoDTO> idProdutoDTOS = Arrays.asList(requestIdProdutoDTO);
        requestLeadDTO.setIdProdutos(idProdutoDTOS);


        Mockito.when(leadRepository.save(Mockito.any())).thenReturn(lead);
        Lead leadObjeto = leadService.salvarLead(requestLeadDTO);

        Assertions.assertEquals(lead, leadObjeto);

    }

}
